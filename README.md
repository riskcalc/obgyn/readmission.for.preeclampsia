# Postpartum readmission for hypertension and pre-eclampsia: development and validation of a predictive model

K K Venkatesh, J E Jelovsek, M Hoffman, A J Beckham, G Bitar, A M Friedman, K A Boggess, D M Stamilio. BJOG. 2023 Jun 14. doi: 10.1111/1471-0528.17572.

PMID: https://pubmed.ncbi.nlm.nih.gov/37317035/

This site provides information and source code for models in the above project.

This study was supported by the University of North Carolina Department of Obstetrics and Gynecology Cefalo-Bowes Grant (Dr Stamilio); National Center for Advancing Translational Sciences (NCATS), National Institutes of Health, through Grant Award Number UL1TR002489; and the Care Innovation and Community Improvement Program at The Ohio State University (Dr Venkatesh).

The copyrights of this software are owned by Duke University. As such, two licenses to this software are offered:
(1) An open-source license under the GPLv2 license.
(2) A custom license with Duke University, for use without the GPLv2 restrictions.

As a recipient of this software, you may choose which license to receive the code under. Outside contributions to the Duke owned code base cannot be accepted unless the contributor transfers the copyright to those changes over to Duke University.
To enter a license agreement without the GPLv2 restrictions, please contact the Digital Innovations department at Duke Office of Licensing and Ventures (https://olv.duke.edu/software/) at olvquestions@duke.edu with reference to “OLV File No. 7484” in your email.

Please note that this software is distributed AS IS, WITHOUT ANY WARRANTY; and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.